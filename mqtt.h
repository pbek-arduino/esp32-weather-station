#include <PubSubClient.h>
WiFiClient espClient;
PubSubClient mqttClient(espClient);

boolean mqttPublish(const char* topic, const char* payload, boolean retained) {
  if (!mqttClient.connected()) {
    Serial.println("MQTT not connected!");
    return false;
  }

  String pTopic = mqtt_topic_prefix;
  pTopic += "/";
  pTopic += topic;
  return mqttClient.publish(pTopic.c_str(), payload, retained);
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String payloadStr;
  String topicStr = topic;
  
  for (int i = 0; i < length; i++) {
    payloadStr += (char)payload[i];
  }
  
  Serial.println(payloadStr);

  if (topicStr.equals("mytopic")) {
    
  }
}

void connectMqtt() {
  Serial.println("Attempting MQTT connection...");

  // attempt to connect
  if (mqttClient.connect("WeatherStation" + random(1000), mqtt_user, mqtt_password)) {
    Serial.println("MQTT connected");

    // set the callback function
    mqttClient.setCallback(mqttCallback);

    // once connected, publish an announcement...
    mqttPublish("Status", "connected", false);

    // subscribe to topics
    mqttClient.subscribe("sometopic");
  } else {
    Serial.print("MQTT failed, rc=");
    Serial.println(mqttClient.state());
  }
}
