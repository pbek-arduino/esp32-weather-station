/* Buttons */
// Pinout: https://randomnerdtutorials.com/esp32-pinout-reference-gpios/
#define BUTTON1_PIN 32
#define BUTTON2_PIN 33
#define BUTTON3_PIN 25

/* Wifi */
const char* ssid = "YOUR_SSID";
const char* password = "YOUR_PW";

/* MQTT */
const char* mqtt_server = "MQTT_IP";
uint mqtt_port = 1883;
const char* mqtt_user = "MQTT_USER";
const char* mqtt_password = "MQTT_PASSWORD";
const char* mqtt_topic_prefix = "HOME/WeatherStation";
