ESP32 Weather Station
=====================

Detects the status of 3 LEDs and publishes it via MQTT.


## Setup

- Copy `config.h.exam` to `config.h` and edit your settings
- [Install ESP32 support in Arduino IDE](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-mac-and-linux-instructions/)
- Select `ESP32 Dev Module` as board in Arduino IDE
