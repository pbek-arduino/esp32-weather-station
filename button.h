/**
 * Buttons that send how long they were pressed via MQTT
 */

#include <DebounceEvent.h>

void buttonCallback(uint8_t pin, uint8_t event, uint8_t count, uint16_t length) {
  Serial.print("Pin : "); Serial.print(pin);
  Serial.print(" Event : "); Serial.print(event);
  Serial.print(" Count : "); Serial.print(count);
  Serial.print(" Length: "); Serial.print(length);
  Serial.println();

  switch (event) {
    case (EVENT_PRESSED):
      switch (pin) {
        case BUTTON1_PIN:
          mqttPublish("Button1Pressed", "", true);
        break;
        case BUTTON2_PIN:
          mqttPublish("Button2Pressed", "", true);
        break;
        case BUTTON3_PIN:
          mqttPublish("Button3Pressed", "", true);
        break;
      };
    break;
    case (EVENT_RELEASED):
      switch (pin) {
        case BUTTON1_PIN:
          mqttPublish("Button1Released", String(length).c_str(), true);
        break;
        case BUTTON2_PIN:
          mqttPublish("Button2Released", String(length).c_str(), true);
        break;
        case BUTTON3_PIN:
          mqttPublish("Button3Released", String(length).c_str(), true);
        break;
      };
    break;
  }
}

DebounceEvent button1 = DebounceEvent(BUTTON1_PIN, buttonCallback, BUTTON_PUSHBUTTON | BUTTON_DEFAULT_HIGH);
DebounceEvent button2 = DebounceEvent(BUTTON2_PIN, buttonCallback, BUTTON_PUSHBUTTON | BUTTON_DEFAULT_HIGH);
DebounceEvent button3 = DebounceEvent(BUTTON3_PIN, buttonCallback, BUTTON_PUSHBUTTON | BUTTON_DEFAULT_HIGH);

void setupButtons() {
  // set input pins and turn on pull-up resistors
  pinMode(BUTTON1_PIN, INPUT_PULLUP);
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  pinMode(BUTTON3_PIN, INPUT_PULLUP);
}

void buttonLoop() {
  button1.loop();
  button2.loop();
  button3.loop();
}
