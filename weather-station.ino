/*****************************************************
 * ESP32 Weather Station
 * 
 * Libraries:
 * https://www.arduino.cc/en/Reference/WiFi
 * https://github.com/knolleary/pubsubclient
 * https://github.com/xoseperez/debounceevent
 *****************************************************/

#include "config.h"
#include "wifi.h"
#include "mqtt.h"
#include "button.h"


void setup() {
  Serial.begin(115200);
  Serial.println("");
  Serial.println("Weather Station (by pbek)");
  Serial.println("");

  mqttClient.setServer(mqtt_server, mqtt_port);
  connectWiFi();
  connectMqtt();
  setupButtons();
}

void loop() {
  // put your main code here, to run repeatedly:


  if (!mqttClient.connected()) {
    connectMqtt();
  }

  mqttClient.loop();
  buttonLoop();
}
